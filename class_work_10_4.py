def read_mat(infile):
    """read files using loadmat or h5py

    :param infile: input file (str)
    :return: matlab x, y, and params (dict)
    """
    from scipy.io import loadmat
    import h5py

    try: 
    	d = loadmat(infile)
    except:
    	f = h5py.File(infile)
    	d = dict(f)

    return d


def read_csv(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import csv
    with open(infile, 'r') as f:
        c = csv.reader(infile)
        for rows in c:
        	k = rows[0]
        	v = rows[1:]
        	class_groups = {k:v for k, v in rows}
        # FIX ME!!
        #class_groups = ????

    return class_groups